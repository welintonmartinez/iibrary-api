const Book = require("../models/bookModel");

// @desc    Gets All books
// @route   GET /api/books
async function getBooks(req, res) {
  try {
    const books = await Book.findAll();
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(JSON.stringify(books));
  } catch (error) {
    console.log(error);
  }
}

// @desc    Gets Single book
// @route   GET /api/book/:id
async function getBook(req, res, id) {
  try {
    const book = await Book.findById(id);

    if (!book) {
      res.writeHead(404, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "Book Not Found" }));
    } else {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(JSON.stringify(book));
    }
  } catch (error) {
    console.log(error);
  }
}

async function getPage(req, res, id, page, format) {
  try {
    const book = await Book.findById(id);
    page = parseInt(page);

    if (!book) {
      res.writeHead(404, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ message: "Book Not Found" }));
      return;
    } else {
      if (page > book.pages.length) {
        res.writeHead(404, { "Content-Type": "application/json" });
        res.end(JSON.stringify({ message: "Page Not Found" }));
        return;
      }

      switch (format) {
        case "":
          res.writeHead(200, { "Content-Type": "text/plain" });
          res.end(book.pages[page - 1]);
          break;
        case "html":
          res.writeHead(200, { "Content-Type": "text/html" });
          res.end(`<p>${book.pages[page - 1]}</p>`);
          break;
      }
    }
  } catch (error) {
    res.writeHead(404, { "Content-Type": "application/json" });
    res.end(JSON.stringify({ message: error }));
    console.log(error);
  }
}

module.exports = {
  getBooks,
  getBook,
  getPage,
};
