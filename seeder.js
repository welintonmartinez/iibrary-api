const Book = require("./models/bookModel");

const stringOfRandomWords =
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum";
const listOfRandomWords = stringOfRandomWords.split(" ");

async function populateDB() {
  for (let bookIndex = 0; bookIndex < 10; bookIndex++) {
    let title = `book${Math.floor(Math.random() * 1000)}`;
    let pageCount = Math.floor(Math.random() * 100);
    let pages = [];

    for (let i = 0; i < pageCount; i++) {
      let sentence = "";
      for (let j = 0; j < 10; j++) {
        let randomWordIndex = Math.floor(
          Math.random() * listOfRandomWords.length
        );
        sentence += listOfRandomWords[randomWordIndex] + " ";
      }
      pages.push(`Page Number ${i + 1}\n ${sentence}`);
    }

    await Book.create({ title, pageCount, pages });
    console.log(`Created book #${bookIndex}`);
  }
}

async function deleteDB() {
  let allBooks = await Book.findAll();
  allBooks.forEach((book) => {
    Book.remove(book.id);
  });
}

if (process.argv[2] === "-p") {
  populateDB();
} else if (process.argv[2] === "-r") {
  deleteDB();
}
