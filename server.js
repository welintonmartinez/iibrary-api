const http = require("http");
const bookController = require("./controllers/bookController");

const server = http.createServer((req, res) => {
  if (req.url === "/api/books" && req.method === "GET") {
    //Get list of books
    bookController.getBooks(req, res);
  } else if (
    req.url.match(/\/api\/books\/.+\/page\/\d+(\/\w+)?$/g) &&
    req.method === "GET"
  ) {
    //Get book page from query. Ex: localhost:5000/api/251354/page/11/html
    const splitUrl = req.url.split("/");
    const id = splitUrl[3];
    const page = splitUrl[5];
    const format = splitUrl[6] || "";
    bookController.getPage(req, res, id, page, format);
  } else if (req.url.match(/\/api\/books\/\w+/) && req.method === "GET") {
    //Get book from ID. Ex: localhost:5000/api/books/251354
    const id = req.url.split("/")[3];
    bookController.getBook(req, res, id);
  } else {
    res.writeHead(404, { "Content-Type": "application/json" });
    res.end(
      JSON.stringify({
        message: "Route Not Found: Please use the api/books endpoint",
      })
    );
  }
});

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
