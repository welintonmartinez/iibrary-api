let books = require("../data/books");
const fs = require("fs");
const { v4: uuidv4 } = require("uuid");

function findAll() {
  return new Promise((resolve, reject) => {
    const cleanedBooks = books.map((book) => {
      return {
        id: book.id,
        title: book.title,
        pageCount: book.pageCount,
      };
    });
    resolve(cleanedBooks);
  });
}

function findById(id) {
  return new Promise((resolve, reject) => {
    const book = books.find((p) => p.id === id);
    resolve(book);
  });
}

function create(book) {
  return new Promise((resolve, reject) => {
    const newbook = { id: uuidv4(), ...book };
    books.push(newbook);
    fs.writeFileSync(
      "./data/books.json",
      JSON.stringify(books, null, 2),
      "utf8",
      (err) => {
        if (err) {
          console.log(err);
          reject(err);
        }
      }
    );
    resolve(newbook);
  });
}

function remove(id) {
  return new Promise((resolve, reject) => {
    books = books.filter((p) => p.id !== id);
    fs.writeFileSync(
      "./data/books.json",
      JSON.stringify(books, null, 2),
      "utf8",
      (err) => {
        if (err) {
          console.log(err);
        }
      }
    );
    resolve();
  });
}

module.exports = {
  findAll,
  findById,
  create,
  remove,
};
