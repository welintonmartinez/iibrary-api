# Library / Biblioteca 

## Inital Setup
To run this Node JS project it is needed to install packages with:
`npm install`

The only package that is required for this solution is **uuid**.

To create a new set of books and populate the database, run the following command:
`node seeder.js -p`

This creates packages of 10 randomly generated books in a JSON file with the following properties: id, title, pageCount and pages. If you run this command again, the new 10 books will append to existing ones. 

To remove all of the books from the database use the following command:
`node seeder.js -r`

To start the server and begin making requests run: `node server.js`

## Routes
To make requests to the server use the following routes:

- **Get list of books:**

To get a full list of all the books in the library use route `localhost:5000/api/books`. This will display the general information from each book in the library, which includes id, title and pageCount.

- **Get a book:**

To get an specific book use a route such as `localhost:5000/api/books/uniqueID`. This will return an specific book with all its properties, including id, title, pageCount, and preliminary display of all its pages.

- **Get a book page in the desired format:**

To get an specific page from a book use a route such as `localhost:5000/api/books/uniqueID/page/54/html`. This will return an specific page from a book in the desired format. A page can be displayed, for now,  in either HTML format, or plain text if you do not specify the desired format (with a route like `localhost:5000/api/books/uniqueID/page/54`).
